# Angular testing utilities
This library contains some utilities which shall help to write unit and e2e tests for angular applications.

## Drag & drop
To test whether drag & drop works properly the `dragAndDrop(sourceElement, destinationElement, options)` function helps to simplify this task. You can use this helper function in e2e and unit tests.

### Unit tests
```typescript
import { async, TestBed } from '@angular/core/testing';
import { TestHostComponent } from './test-host.component';
import { dragAndDrop } from 'angular-testing-utils';

beforeEach(async(() => {
  TestBed.configureTestingModule({
    declarations: [
      TestHostComponent,
    ]
  }).compileComponents();
}));

it('should drag & drop', () => {
  // Arrange
  const fixture = TestBed.createComponent(TestHostComponent);
  const draggable = fixture.nativeElement.querySelector('app-draggable');
  const dropZone = fixture.nativeElement.querySelector('app-drop-zone');
    
  // Act
  dragAndDrop(draggable, dropZone);
  
  fixture.detectChanges();
  
  // Assert
  // expect(dropZone).to...
});
``` 

### e2e tests
```typescript
import { browser, by, element } from 'protractor';
import { dragAndDrop } from 'angular-testing-utils';

it('should drag & drop', async () => {
  // Arrange
  const draggable = element(by.id('draggable'));
  const dropZone = element(by.id('drop-zone'));
  
  // Act
  await browser.executeScript(dragAndDrop, draggable.getWebElement(), dropZone.getWebElement());
  
  // Assert
  expect(dropZone.element(by.id('draggable')).isPresent()).toBe(true);
});
```

### Capture drag events
If it is required to get all triggered drag events the `captureDragEvents(sourceElement, destinationElement): Observable<DragEvent>` function can be used to listen for these events. It must be called before the `dragAndDrop()` function.

```typescript
import { dragAndDrop, captureDragEvents } from 'angular-testing-utils';
import { toArray } from 'rxjs/operators';

it('should capture drag events', async () => {
  // Arrange
  const draggable = fixture.nativeElement.querySelector('app-draggable');
  const dropZone = fixture.nativeElement.querySelector('app-drop-zone');
    
  // Act
  const events = captureDragEvents(draggable, dropZone)
    .pipe(toArray())
    .toPromise();
  
  dragAndDrop(draggable, dropZone);
  
  const actualEvents = await events;
  
  // Assert
  expect(actualEvents.length).toBeGreaterThan(0);
  expect(actualEvents[0].type).toEqual('dragstart');
});
```

#### Operators
There are some rx operator which can help to find specific events.

##### filterBy
The `filterBy(criterion)` operator filters the event stream by the given criterion which can be a `HTMLElement` or the specific event type.

```typescript
import { captureDragEvents, filterBy } from 'angular-testing-utils';

// filters by the given HTMLElement
captureDragEvents(draggable, dropZone)
  .pipe(
    filterBy(draggable)
  );

// filters by the given drag event type
captureDragEvents(draggable, dropZone)
  .pipe(
    filterBy('dragenter')
  );
```

##### firstWhere & lastWhere
These two operators gets the first or last drag event in the stream.

```typescript
import { captureDragEvents, firstWhere, lastWhere } from 'angular-testing-utils';

// gets first drag event where target matches the dropZone element
captureDragEvents(draggable, dropZone)
  .pipe(
    firstWhere(dropZone)
  );

// gets the last drag event where the targets event type matches 'drag'
captureDragEvents(draggable, dropZone)
  .pipe(
    lastWhere('drag')
  );
```