import { browser, by, element, ElementFinder } from 'protractor';
import { dragAndDrop } from '../lib';

describe('', () => {
  let draggable1Element: ElementFinder;
  let draggable2Element: ElementFinder;
  let dropZone1Element: ElementFinder;
  let dropZone2Element: ElementFinder;

  beforeEach(async () => {
    await browser.waitForAngularEnabled(false);
    await browser.get('/');

    draggable1Element = element(by.id('draggable-1'));
    draggable2Element = element(by.id('draggable-2'));
    dropZone1Element = element(by.id('drop-zone-1'));
    dropZone2Element = element(by.id('drop-zone-2'));
  });

  it('should navigate', async () => {
    // Act
    const dropZone1ChildrenCount = await dropZone1Element.all(by.css('.draggable')).count();
    const dropZone2ChildrenCount = await dropZone2Element.all(by.css('.draggable')).count();

    // Assert
    expect(dropZone1ChildrenCount).toEqual(0);
    expect(dropZone2ChildrenCount).toEqual(0);
  });

  it('should drag and drop element', async () => {
    // Act
    await browser.executeScript(dragAndDrop, draggable1Element.getWebElement(), dropZone1Element.getWebElement());
    const actualChildrenCount = await dropZone1Element.all(by.css('.draggable')).count();

    // Assert
    expect(actualChildrenCount).toEqual(1);
  });

  it('should drag and drop multiple draggable elements', async () => {
    // Act
    await browser.executeScript(dragAndDrop, draggable1Element.getWebElement(), dropZone1Element.getWebElement());
    await browser.executeScript(dragAndDrop, draggable2Element.getWebElement(), dropZone1Element.getWebElement());
    const actualChildrenCount = await dropZone1Element.all(by.css('.draggable')).count();

    // Assert
    expect(actualChildrenCount).toEqual(2);
  });

  it('should drag and drop elements into different drop areas', async () => {
    // Act
    await browser.executeScript(dragAndDrop, draggable1Element.getWebElement(), dropZone1Element.getWebElement());
    await browser.executeScript(dragAndDrop, draggable2Element.getWebElement(), dropZone2Element.getWebElement());
    const dropZone1ChildrenCount = await dropZone1Element.all(by.css('.draggable')).count();
    const dropZone2ChildrenCount = await dropZone2Element.all(by.css('.draggable')).count();

    // Assert
    expect(dropZone1ChildrenCount).toEqual(1);
    expect(dropZone2ChildrenCount).toEqual(1);
  });

});
