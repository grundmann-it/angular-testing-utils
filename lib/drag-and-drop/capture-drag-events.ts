import { fromEvent, merge, Observable } from 'rxjs';
import { filter, first, last, take } from 'rxjs/operators';
import { DragEventType } from './types';

function fromEventOnce<T>(target: HTMLElement, eventName: string): Observable<T> {
  return fromEvent<T>(target, eventName, { once: true })
    .pipe(
      take(1)
    );
}

function filterByType<T extends Event>(type: DragEventType): (source: Observable<T>) => Observable<T> {
  return filter(event => event.type === type);
}

function filterByTarget<T extends Event>(target: HTMLElement): (source: Observable<T>) => Observable<T> {
  return filter(event => event.target === target);
}

export function filterBy<T extends Event>(criterion: HTMLElement | DragEventType): (source: Observable<T>) => Observable<T> {
  return criterion instanceof HTMLElement ? filterByTarget(criterion) : filterByType(criterion);
}

export function firstWhere<T extends Event>(criterion: HTMLElement | DragEventType): (source: Observable<T>) => Observable<T> {
  return (source) => {
    return source.pipe(
      filterBy(criterion),
      first()
    );
  }
}

export function lastWhere<T extends Event>(criterion: HTMLElement | DragEventType): (source: Observable<T>) => Observable<T> {
  return (source) => {
    return source.pipe(
      filterBy(criterion),
      last()
    );
  }
}

export function captureDragEvents(sourceElement: HTMLElement, destinationElement: HTMLElement): Observable<DragEvent> {
  return merge<DragEvent>(
    fromEventOnce(sourceElement, 'dragstart'),
    fromEventOnce(sourceElement, 'drag'),
    fromEventOnce(sourceElement, 'dragend'),
    fromEventOnce(destinationElement, 'dragenter'),
    fromEventOnce(destinationElement, 'dragover'),
    fromEventOnce(destinationElement, 'drop'),
    fromEventOnce(destinationElement, 'dragend'),
  );
}
