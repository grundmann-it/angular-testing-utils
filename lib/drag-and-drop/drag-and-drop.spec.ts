import { toArray } from 'rxjs/operators';
import { captureDragEvents, filterBy, firstWhere, lastWhere } from './capture-drag-events';
import { dragAndDrop } from './drag-and-drop';

interface EventListener {
  onDragStart(event: DragEvent): void;

  onDrag(event: DragEvent): void;

  onDragEnter(event: DragEvent): void;

  onDragOver(event: DragEvent): void;

  onDrop(event: DragEvent): void;

  onDragLeave(event: DragEvent): void;

  onDragEnd(event: DragEvent): void;
}

function addEventListener(spyName: string, element: HTMLElement): EventListener {
  const spy: EventListener = jasmine.createSpyObj(spyName, [
    'onDragStart', 'onDrag', 'onDragEnter', 'onDragOver', 'onDrop', 'onDragLeave', 'onDragEnd'
  ]);

  element.addEventListener('dragstart', spy.onDragStart);
  element.addEventListener('drag', spy.onDrag);
  element.addEventListener('dragenter', spy.onDragEnter);
  element.addEventListener('dragover', spy.onDragOver);
  element.addEventListener('drop', spy.onDrop);
  element.addEventListener('dragleave', spy.onDragLeave);
  element.addEventListener('dragend', spy.onDragEnd);

  return spy;
}

describe('dragAndDrop', () => {
  let draggableElement: HTMLElement;
  let dropZoneElement: HTMLElement;
  let draggableEventListenerSpy: EventListener;
  let dropZoneEventListenerSpy: EventListener;

  beforeEach(() => {
    const parentElement = document.body;
    draggableElement = document.createElement('span');
    dropZoneElement = document.createElement('div');

    parentElement.appendChild(draggableElement);
    parentElement.appendChild(dropZoneElement);

    draggableEventListenerSpy = addEventListener('DraggableEventHandler', draggableElement);
    dropZoneEventListenerSpy = addEventListener('DropZoneEventHandler', dropZoneElement);
  });

  describe('captureDragEvents', function () {

    it('should capture all drag events', async () => {
      // Act
      const events = captureDragEvents(draggableElement, dropZoneElement)
        .pipe(
          toArray()
        )
        .toPromise();

      dragAndDrop(draggableElement, dropZoneElement);

      const [
        draggableDragStartEvent,
        draggableDragEvent,
        dropZoneDragEnterEvent,
        dropZoneDragOverEvent,
        dropZoneDropEvent,
        dropZoneDragEndEvent,
        draggableDragEndEvent
      ] = await events;

      // Assert
      expect(draggableDragStartEvent.type).toEqual('dragstart');
      expect(draggableDragStartEvent.target).toEqual(draggableElement);

      expect(draggableDragEvent.type).toEqual('drag');
      expect(draggableDragEvent.target).toEqual(draggableElement);

      expect(dropZoneDragEnterEvent.type).toEqual('dragenter');
      expect(dropZoneDragEnterEvent.target).toEqual(dropZoneElement);

      expect(dropZoneDragOverEvent.type).toEqual('dragover');
      expect(dropZoneDragOverEvent.target).toEqual(dropZoneElement);

      expect(dropZoneDropEvent.type).toEqual('drop');
      expect(dropZoneDropEvent.target).toEqual(dropZoneElement);

      expect(dropZoneDragEndEvent.type).toEqual('dragend');
      expect(dropZoneDragEndEvent.target).toEqual(dropZoneElement);

      expect(draggableDragEndEvent.type).toEqual('dragend');
      expect(draggableDragEndEvent.target).toEqual(draggableElement);
    });

    it('should filter events by its type', async () => {
      // Act
      const events = captureDragEvents(draggableElement, dropZoneElement)
        .pipe(
          filterBy('dragenter'),
          toArray(),
        )
        .toPromise();

      dragAndDrop(draggableElement, dropZoneElement);

      const actualEvents = await events;
      const [dragEnterEvent] = actualEvents;

      // Assert
      expect(actualEvents.length).toEqual(1);
      expect(dragEnterEvent.type).toEqual('dragenter');
      expect(dragEnterEvent.target).toEqual(dropZoneElement);
    });

    it('should filter events by its target', async () => {
      // Act
      const events = captureDragEvents(draggableElement, dropZoneElement)
        .pipe(
          filterBy(draggableElement),
          toArray(),
        )
        .toPromise();

      dragAndDrop(draggableElement, dropZoneElement);

      const actualEvents = await events;
      const [dragStartEvent, dragEvent, dragEndEvent] = actualEvents;

      // Assert
      expect(actualEvents.length).toEqual(3);
      expect(dragStartEvent.type).toEqual('dragstart');
      expect(dragStartEvent.target).toEqual(draggableElement);
      expect(dragEvent.type).toEqual('drag');
      expect(dragEvent.target).toEqual(draggableElement);
      expect(dragEndEvent.type).toEqual('dragend');
      expect(dragEndEvent.target).toEqual(draggableElement);
    });

    it('should get first event where target satisfies the given element', async () => {
      // Act
      const event = captureDragEvents(draggableElement, dropZoneElement)
        .pipe(
          firstWhere(dropZoneElement),
        )
        .toPromise();

      dragAndDrop(draggableElement, dropZoneElement);

      const actualEvent = await event;

      // Assert
      expect(actualEvent.type).toEqual('dragenter');
      expect(actualEvent.target).toEqual(dropZoneElement);
    });

    it('should get last event where target satisfies the given element', async () => {
      // Act
      const event = captureDragEvents(draggableElement, dropZoneElement)
        .pipe(
          lastWhere(draggableElement),
        )
        .toPromise();

      dragAndDrop(draggableElement, dropZoneElement);

      const actualEvent = await event;

      // Assert
      expect(actualEvent.type).toEqual('dragend');
      expect(actualEvent.target).toEqual(draggableElement);
    });

    it('should get first event where target satisfies the given type', async () => {
      // Act
      const event = captureDragEvents(draggableElement, dropZoneElement)
        .pipe(
          firstWhere('dragover'),
        )
        .toPromise();

      dragAndDrop(draggableElement, dropZoneElement);

      const actualEvent = await event;

      // Assert
      expect(actualEvent.type).toEqual('dragover');
      expect(actualEvent.target).toEqual(dropZoneElement);
    });

    it('should get last event where target satisfies the given type', async () => {
      // Act
      const event = captureDragEvents(draggableElement, dropZoneElement)
        .pipe(
          lastWhere('dragend'),
        )
        .toPromise();

      dragAndDrop(draggableElement, dropZoneElement);

      const actualEvent = await event;

      // Assert
      expect(actualEvent.type).toEqual('dragend');
      expect(actualEvent.target).toEqual(draggableElement);
    });

  });

  describe('draggable', function () {

    it('should trigger "dragstart" event', () => {
      // Act
      dragAndDrop(draggableElement, dropZoneElement);

      // Assert
      expect(draggableEventListenerSpy.onDragStart).toHaveBeenCalled();
    });

    it('should trigger "drag" event', () => {
      // Act
      dragAndDrop(draggableElement, dropZoneElement);

      // Assert
      expect(draggableEventListenerSpy.onDrag).toHaveBeenCalled();
    });

    it('should trigger "dragend" event', () => {
      // Act
      dragAndDrop(draggableElement, dropZoneElement);

      // Assert
      expect(draggableEventListenerSpy.onDragEnd).toHaveBeenCalled();
    });

    it('should not trigger other events', () => {
      // Act
      dragAndDrop(draggableElement, dropZoneElement);

      // Assert
      expect(draggableEventListenerSpy.onDragEnter).not.toHaveBeenCalled();
      expect(draggableEventListenerSpy.onDragOver).not.toHaveBeenCalled();
      expect(draggableEventListenerSpy.onDrop).not.toHaveBeenCalled();
      expect(draggableEventListenerSpy.onDragLeave).not.toHaveBeenCalled();
    });

    it('should trigger events in correct order', () => {
      // Act
      dragAndDrop(draggableElement, dropZoneElement);

      // Assert
      expect(draggableEventListenerSpy.onDragStart).toHaveBeenCalledBefore(draggableEventListenerSpy.onDrag as jasmine.Spy);
      expect(draggableEventListenerSpy.onDrag).toHaveBeenCalledBefore(draggableEventListenerSpy.onDragEnd as jasmine.Spy);
    });
  });

  describe('dropZone', function () {

    it('should trigger "dragenter" event', () => {
      // Act
      dragAndDrop(draggableElement, dropZoneElement);

      // Assert
      expect(dropZoneEventListenerSpy.onDragEnter).toHaveBeenCalled();
    });

    it('should trigger "dragover" event', () => {
      // Act
      dragAndDrop(draggableElement, dropZoneElement);

      // Assert
      expect(dropZoneEventListenerSpy.onDragOver).toHaveBeenCalled();
    });

    it('should trigger "drop" event', () => {
      // Act
      dragAndDrop(draggableElement, dropZoneElement);

      // Assert
      expect(dropZoneEventListenerSpy.onDrop).toHaveBeenCalled();
    });

    it('should trigger "dragend" event', () => {
      // Act
      dragAndDrop(draggableElement, dropZoneElement);

      // Assert
      expect(dropZoneEventListenerSpy.onDragEnd).toHaveBeenCalled();
    });

    it('should not trigger other events', () => {
      // Act
      dragAndDrop(draggableElement, dropZoneElement);

      // Assert
      expect(dropZoneEventListenerSpy.onDragStart).not.toHaveBeenCalled();
      expect(dropZoneEventListenerSpy.onDrag).not.toHaveBeenCalled();
      expect(dropZoneEventListenerSpy.onDragLeave).not.toHaveBeenCalled();
    });

    it('should trigger events in correct order', () => {
      // Act
      dragAndDrop(draggableElement, dropZoneElement);

      // Assert
      expect(dropZoneEventListenerSpy.onDragEnter).toHaveBeenCalledBefore(dropZoneEventListenerSpy.onDragOver as jasmine.Spy);
      expect(dropZoneEventListenerSpy.onDragOver).toHaveBeenCalledBefore(dropZoneEventListenerSpy.onDrop as jasmine.Spy);
      expect(dropZoneEventListenerSpy.onDrop).toHaveBeenCalledBefore(dropZoneEventListenerSpy.onDragEnd as jasmine.Spy);
    });
  });

});
