import { DragEventType, DropEffect, EffectAllowed, EventOptions } from './types';

export function dragAndDrop(sourceElement: HTMLElement, destinationElement: HTMLElement, options?: EventOptions): void {
  const { clientY = 0, clientX = 0 } = options || {};

  class InMemoryDataTransferItemList implements DataTransferItemList {
    [name: number]: DataTransferItem;

    readonly length: number = 0;

    add(data: File): DataTransferItem | null;
    add(data: string, type: string): DataTransferItem | null;
    add(data: File | string, type?: string): DataTransferItem | null {
      return null;
    }

    clear(): void {
    }

    item(index: number): DataTransferItem {
      return this[index];
    }

    remove(index: number): void {
    }

  }

  class InMemoryFileList implements FileList {
    [index: number]: File;

    readonly length: number = 0;

    item(index: number): File | null {
      return this[index];
    }
  }

  class InMemoryDataTransfer implements DataTransfer {
    private readonly data: { [format: string]: any } = {};

    dropEffect: DropEffect = 'none';
    effectAllowed: EffectAllowed = 'uninitialized';
    readonly files: FileList = new InMemoryFileList();
    readonly items: DataTransferItemList = new InMemoryDataTransferItemList();
    readonly types: string[] = [];

    clearData(format?: string): boolean {
      return false;
    }

    getData(format: string): string {
      return this.data[format];
    }

    setData(format: string, data: string): boolean {
      this.data[format] = data;

      return true;
    }

    setDragImage(image: Element, x: number, y: number): void {
    }

  }

  /**
   * Creates a new DragEvent
   * @param {HTMLElement} target
   * @param {DragEventType} type
   * @param {DataTransfer} dataTransfer
   * @returns {DragEvent}
   */
  function createDragEvent(target: HTMLElement,
                           type: DragEventType,
                           dataTransfer: DataTransfer): DragEvent {
    const event = document.createEvent('DragEvent');
    event.initEvent(type, true, true);

    event.initMouseEvent(
      type,
      true,
      true,
      null,
      0,
      0,
      0,
      clientX,
      clientY,
      false,
      false,
      false,
      false,
      0,
      null
    );

    Object.defineProperty(event, 'dataTransfer', { get: () => dataTransfer });
    return event;
  }

  /**
   * Dispatches an DragEvent at the given target
   * @param {HTMLElement} target
   * @param {DragEvent} type
   * @param {DataTransfer} dataTransfer
   */
  function dispatchDragEvent(target: HTMLElement,
                             type: DragEventType,
                             dataTransfer: DataTransfer): void {
    const event = createDragEvent(target, type, dataTransfer);
    target.dispatchEvent(event);
  }

  const dataTransfer = new InMemoryDataTransfer();

  dispatchDragEvent(sourceElement, 'dragstart', dataTransfer);
  dispatchDragEvent(sourceElement, 'drag', dataTransfer);
  dispatchDragEvent(destinationElement, 'dragenter', dataTransfer);
  dispatchDragEvent(destinationElement, 'dragover', dataTransfer);
  dispatchDragEvent(destinationElement, 'drop', dataTransfer);
  dispatchDragEvent(destinationElement, 'dragend', dataTransfer);
  dispatchDragEvent(sourceElement, 'dragend', dataTransfer);
}
