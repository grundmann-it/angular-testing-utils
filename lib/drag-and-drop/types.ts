export type DropEffect = 'copy' | 'move' | 'link' | 'none';

export type EffectAllowed =
  'none'
  | 'copy'
  | 'copyLink'
  | 'copyMove'
  | 'link'
  | 'linkMove'
  | 'move'
  | 'all'
  | 'uninitialized';

export type DragEventType =
  'dragstart'
  | 'dragenter'
  | 'dragend'
  | 'dragover'
  | 'dragleave'
  | 'drag'
  | 'drop';

export interface EventOptions {
  readonly clientX?: number;
  readonly clientY?: number;
}