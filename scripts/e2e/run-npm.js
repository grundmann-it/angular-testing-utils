const {spawn} = require('child_process');

const npmRun = function(script) {
    return new Promise(resolve => {
        const cmd = spawn(/^win/.test(process.platform) ? 'npm.cmd' : 'npm', ['run', script]);

        cmd.stdout.on('data', (data) => {
            console.log(data.toString());
        });

        cmd.stderr.on('data', (data) => {
            console.error(data.toString());
        });

        cmd.on('close', (code) => {
            resolve(code);
        });
    });
};

module.exports = npmRun;
