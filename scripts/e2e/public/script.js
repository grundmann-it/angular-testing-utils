const dropZoneElements = document.querySelectorAll('.drop-zone');
const draggableElements = document.querySelectorAll('.draggable');

const type = 'text';

// polyfill for ie11
if (typeof NodeList.prototype.forEach !== 'function') {
    NodeList.prototype.forEach = function (callback) {
        for (let i = 0; i < this.length; i++) {
            callback(this[i]);
        }
    };
}

function containsType(types, type) {
    if (types instanceof DOMStringList) {
        return types.contains(types);
    } else if (types instanceof Array) {
        return types.indexOf(type) !== -1;
    }

    return false;
}

dropZoneElements.forEach(function (dropZone) {

    dropZone.addEventListener('dragenter', function (event) {
        if (containsType(event.dataTransfer.types, type)) {
            event.preventDefault();
        }
    });

    dropZone.addEventListener('dragover', function (event) {
        event.dataTransfer.dropEffect = 'move';
        event.preventDefault();
    });

    dropZone.addEventListener('drop', function (event) {
        event.preventDefault();

        const id = event.dataTransfer.getData(type);
        const draggable = document.querySelector("#" + id);

        draggable.parentElement.removeChild(draggable);
        dropZone.appendChild(draggable);
    });
});

draggableElements.forEach(function (draggable) {
    draggable.addEventListener('dragstart', function (event) {
        event.dataTransfer.effectAllowed = 'all';
        event.dataTransfer.setData(type, event.target.id);
    });
});
