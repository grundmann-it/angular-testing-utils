const express = require('express');
const path = require('path');
const app = express();

const staticDir = path.join(__dirname, 'public');

app.use(express.static(staticDir));

const startServer = function() {
    return new Promise(resolve => {
        app.listen(3000, function () {
            console.log('e2e');
            resolve();
        });
    });
};

module.exports = startServer;
