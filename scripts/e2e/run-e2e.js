const npmRun = require("./run-npm");
const startServer = require('./server');

function runE2e() {
  return startServer()
      .then(() => npmRun('protractor'))
      .then(code => process.exit(code));
}

runE2e();
