// Karma configuration file, see link for more information
// https://karma-runner.github.io/1.0/config/configuration-file.html

module.exports = function (config) {
    config.set({
        basePath: '',
        frameworks: [
            'jasmine',
            'jasmine-matchers',
            'karma-typescript'
        ],
        plugins: [
            require('karma-jasmine'),
            require('karma-jasmine-matchers'),
            require('karma-chrome-launcher'),
            require('karma-firefox-launcher'),
            require('karma-ie-launcher'),
            require('karma-mocha-reporter'),
            require('karma-jasmine-html-reporter'),
            require('karma-coverage-istanbul-reporter'),
            require('karma-typescript')
        ],
        karmaTypescriptConfig: {
            compilerOptions: {
                target: 'es5',
                lib: ['dom', 'es2017'],
            },
            bundlerOptions: {
                transforms: [require("karma-typescript-es6-transform")()]
            }
        },
        preprocessors: {
            '**/*.ts': 'karma-typescript'
        },
        client: {
            clearContext: false // leave Jasmine Spec Runner output visible in browser
        },
        files: [
            {pattern: './node_modules/es6-shim/es6-shim.js'},
            './lib/**/*.ts'
        ],
        coverageIstanbulReporter: {
            dir: require('path').join(__dirname, 'coverage'),
            reports: ['text', 'lcov'],
            fixWebpackSourcePaths: true,
            thresholds: {
                emitWarning: false,
                global: {
                    statements: 90,
                    lines: 90,
                    branches: 90,
                    functions: 90
                },
                each: {
                    statements: 90,
                    lines: 90,
                    branches: 90,
                    functions: 90,
                    overrides: {
                        '**/*.mock.ts': {
                            statements: 0,
                            lines: 0,
                            branches: 0,
                            functions: 0,
                        },
                        '**/*.po.ts': {
                            statements: 0,
                            lines: 0,
                            branches: 0,
                            functions: 0,
                        }
                    }
                }
            },
        },
        reporters: ['mocha', 'kjhtml'],
        port: 9876,
        colors: true,
        logLevel: config.LOG_INFO,
        autoWatch: true,
        browsers: [
            'ChromeHeadlessNoSandbox',
            'FirefoxHeadless',
            'IE11'
        ],
        singleRun: false,
        customLaunchers: {
            ChromeHeadlessNoSandbox: {
                base: 'ChromeHeadless',
                flags: ['--no-sandbox']
            },
            FirefoxHeadless: {
                base: 'Firefox',
                flags: ['-headless']
            },
            IE11: {
                base: 'IE',
                'x-ua-compatible': 'IE=EmulateIE11'
            }
        },
    });
};
